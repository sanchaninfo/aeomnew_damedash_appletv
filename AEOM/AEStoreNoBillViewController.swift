//
//  NoBillViewController.swift
//  PumaStore
//
//  Created by Sanchan on 01/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class AEStoreNoBillViewController: UIViewController {

    @IBOutlet var gotoLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        gotoLbl.text = "Go to \(kBaseUrl)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okbtn(_ sender: Any) {
     let _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
