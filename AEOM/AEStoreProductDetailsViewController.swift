//
//  AEStoreProductDetailsViewController.swift
//  PumaStore
//
//  Created by Sanchan on 21/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class AEStoreProductDetailsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    var productlist = NSDictionary()
    @IBOutlet weak var largeImage: UIImageView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var destitle: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var shopview: UIView!
    @IBOutlet weak var shopBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var userId = String()
    var uuid,deviceId: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoImg = "http://s3.amazonaws.com/peafowl/aeom/cover.jpg"
        imageLogo.kf.setImage(with: URL(string: logoImg))
        priceLbl.text = "$" + "  " +  "\(productlist["price"] as! String)"
        shopBtn.layer.cornerRadius = 7.0
        let topButtonFocusGuide = UIFocusGuide()
        if #available(tvOS 10.0, *) {
            topButtonFocusGuide.preferredFocusEnvironments = [shopBtn]
        } else {
            // Fallback on earlier versions
        }
        self.view.addLayoutGuide(topButtonFocusGuide)
        self.view.addConstraints([topButtonFocusGuide.topAnchor.constraint(equalTo: shopview.topAnchor), topButtonFocusGuide.bottomAnchor.constraint(equalTo: shopview.bottomAnchor), topButtonFocusGuide.leadingAnchor.constraint(equalTo: shopview.leadingAnchor), topButtonFocusGuide.widthAnchor.constraint(equalTo: shopview.widthAnchor)])
        
        // Do any additional setup after loading the view.
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (productlist["images"] as! NSArray).count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let image1 = (productlist["images"] as! NSArray)[indexPath.row] as! NSDictionary
        let image = kStoreBaseUrl + (image1["thumb"] as! String)
        let img = (cell.viewWithTag(10) as! UIImageView)
        img.kf.indicatorType = .activity
        (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: image))
        destitle.text = (productlist["description"] as! String)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
   
            (cell.viewWithTag(10) as! UIImageView).transform = .identity
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
                let image1 = (productlist["images"] as! NSArray)[next.row] as! NSDictionary
                let image = kStoreBaseUrl + (image1["large"] as! String)
                (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                largeImage.kf.setImage(with: URL(string: image))
        }
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == self.shopBtn
        {
            shopBtn.layer.borderColor = focusColor
            shopBtn.layer.borderWidth = 5.0
        }
        if context.previouslyFocusedView == self.shopBtn
        {
            shopBtn.layer.borderWidth = 0.0
        }
    }

    @IBAction func shopBtn(_ sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productdetailpage = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! AEStoreDetailViewController
        productdetailpage.productlist = productlist
        productdetailpage.userId = userId
        productdetailpage.uuid = uuid
        productdetailpage.deviceId = deviceId
        self.navigationController?.pushViewController(productdetailpage, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
