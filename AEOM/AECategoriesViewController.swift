
//
//  ASCategoriesViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class AECategoriesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    var CarousalData = [[String:Any]]()
    var CategoryCarousalName = NSMutableArray()
    var menuCollectionList = NSMutableArray()
    var UserId,uuid,deviceId:String!
    var MyListthumb = [[String:Any]]()
    var categoryDelegate:myListdelegate!
    var insetButton : UIButton = UIButton()
    var nextButton: UIButton = UIButton()
    var PhotoDict = [[String:Any]]()
    var storeProdData = [[String:Any]]()
    var isPhoto = Bool()
    var tableindex = Int()
    private  var rightHandFocusGuide = UIFocusGuide()
    
    @IBOutlet var likeView: UIView!
    @IBOutlet weak var listcollectionview: UICollectionView!
    @IBOutlet weak var backgroundimg: UIImageView!
    @IBOutlet weak var focusButton: UIButton!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieTime: UILabel!
    @IBOutlet weak var rightHandView:UIView!
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var lftImg: UIImageView!
    @IBOutlet var Btn: UIButton!
    var viewToFocus: UIView? = nil
        {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override weak var preferredFocusedView: UIView?
    {
        if viewToFocus != nil
        {
            return viewToFocus;
        }
        else
        {
            return super.preferredFocusedView
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Btn.isHidden = true
        Btn.isEnabled = false
        lftImg.isHidden = true
        likeView.isHidden = true
        insetButton.frame = CGRect(x: 0, y: 630, width: 10, height: 10)
        insetButton.backgroundColor = UIColor.clear
        insetButton.layer.borderWidth = 1.0
        insetButton.layer.borderColor = UIColor.clear.cgColor
        insetButton.setTitle("Press This", for: .normal)
        insetButton.setTitle("Highlighted This", for: .highlighted)
        insetButton.setTitle("Selected This", for: .selected)
        insetButton.setTitle("Focused This", for: .focused)
        insetButton.setTitleColor(UIColor.clear, for: .normal)
        rightHandView.addSubview(insetButton)
//        nextButton.frame = CGRect(x: 0, y: 830, width: 10, height: 10)
//        nextButton.backgroundColor = UIColor.clear
//        nextButton.layer.borderWidth = 1.0
//        nextButton.layer.borderColor = UIColor.clear.cgColor
//        rightHandView.addSubview(nextButton)
        view.addLayoutGuide(rightHandFocusGuide)
        
        rightHandFocusGuide.bottomAnchor.constraint(equalTo: tableview.bottomAnchor).isActive = true
        rightHandFocusGuide.leftAnchor.constraint(equalTo: tableview.rightAnchor).isActive = true
        rightHandFocusGuide.topAnchor.constraint(equalTo: tableview.topAnchor).isActive = true
        rightHandFocusGuide.rightAnchor.constraint(equalTo: rightHandView.leftAnchor).isActive = true
        rightHandFocusGuide.preferredFocusedView = listcollectionview
        textView.isHidden = true
        textView.isUserInteractionEnabled = false
    }
    
    //Number of section
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    //Number of rows in a Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return CategoryCarousalName.count
    }
    
    //Cell Item at IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(10) as! UILabel).text = (CategoryCarousalName[indexPath.row] as! String)
      //  cell.textLabel?.text = CategoryCarousalName[indexPath.row] as? String
      //  cell.textLabel?.font = UIFont.init(name: "Helvetica", size: 29.0)
      //  cell.textLabel?.textColor = UIColor.white
        cell.selectionStyle = .none
        cell.layer.cornerRadius = 8.0
        return cell
    }
    
    //Did Update Focus
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        guard let nextView = context.nextFocusedView else { return }
        if (nextView == insetButton)
        {
            guard let indexPath = context.previouslyFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath) else {
                return
            }
            (cell.viewWithTag(10) as! UILabel).textColor = UIColor.white
           // cell.textLabel?.textColor = UIColor.white
          //  rightHandFocusGuide.preferredFocusedView = cell
            viewToFocus = listcollectionview
            tableView.isHidden = true
            tableindex = indexPath.row
            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x - 300, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
            Btn.isHidden = false
            lftImg.isHidden = false
            Btn.isEnabled = true
            insetButton.isEnabled = false
            
        }
        else
        {
//            if nextView == Btn
//            {
            
//            if nextView == Btn
//            {
//                rightHandView.frame = CGRect(x: rightHandView.frame.origin.x, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
//                tableView.isHidden = false
//                rightHandFocusGuide.preferredFocusedView = insetButton
//            }
//            else
//            {
                rightHandView.frame = CGRect(x: rightHandView.frame.origin.x, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
                tableView.isHidden = false
                rightHandFocusGuide.preferredFocusedView = insetButton
           // }
         //   }
            
        }
        guard let indexPath = context.nextFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath)
            else { return }
    //    cell.textLabel?.textColor = UIColor.black
         (cell.viewWithTag(10) as! UILabel).textColor = UIColor.black
        let selectedCarousal = self.CategoryCarousalName[(indexPath as NSIndexPath).row] as! String
        if selectedCarousal == "Photos"
        {
            isPhoto = true
            categoriesSelected(carousalName: selectedCarousal, carousalDetailDict: PhotoDict)
        }
        else if selectedCarousal == "Store"
        {
            isPhoto = false
            categoriesSelected(carousalName: selectedCarousal, carousalDetailDict: storeProdData)
        }
        else
        {
            categoriesSelected(carousalName: selectedCarousal,carousalDetailDict: CarousalData)

        }
            guard let prevIndexPath = context.previouslyFocusedIndexPath, let prevCell = tableView.cellForRow(at: prevIndexPath)
            else { return }
       // prevCell.textLabel?.textColor = UIColor.white
         (prevCell.viewWithTag(10) as! UILabel).textColor = UIColor.white
        textView.isHidden = true
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == Btn
        {
            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x + 300, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
            tableview.isHidden = false
            Btn.isHidden = true
            lftImg.isHidden = true
            viewToFocus = tableview.cellForRow(at: IndexPath(row: tableindex, section: 0))
            insetButton.isEnabled = true
            movieYear.text = ""
            movieTime.text = ""
            movieTitle.text = ""
            likeView.isHidden = true
           // rightHandFocusGuide.preferredFocusedView = tableview
        }
        
    }
    
    // Number of section in listcollectionview
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    // Number of rows in listcollectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuCollectionList.count
    }
    
    //Cell for Item in listcollectionview
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var image = NSDictionary()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1" , for: indexPath)
        let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
        if imagePath["metadata"] != nil
        {
            image = imagePath["metadata"] as! NSDictionary
            let img = (cell.viewWithTag(10) as! UIImageView)
            img.kf.indicatorType = .activity
            (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string:isnil(json: image, key: kMovieart)))
        }
        else
        {
            if isPhoto == true
            {
                (cell.viewWithTag(10) as! UIImageView).kf.indicatorType = .activity
                (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["thumb"] as! String))))
            }
            else
            {
                (cell.viewWithTag(10) as! UIImageView).kf.indicatorType = .activity
                (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: imagePath["posterURL"] as! String))
            }
        }
        switch indexPath.row
        {
        case 0:
            if imagePath["metadata"] != nil
            {
                
                textView.isHidden = false
                backgroundimg.kf.setImage(with: URL(string: isnil(json: image, key: "main_carousel_image_url")))
                
             /*   movieTitle.text = (isnil(json: imagePath, key: "name")).capitalized
                
                movieTitle?.frame = CGRect(x: (movieTitle?.frame.origin.x)!, y: (movieTitle?.frame.origin.y)!, width: ((movieTitle.text)?.widthWithConstrainedWidth(height: 60, font: (movieTitle?.font)!))!, height: (movieTitle?.frame.size.height)!)
                let str1 = ((isnil(json: image, key: "release_date")).components(separatedBy: "-"))
                movieYear.text = str1[0]
                movieTime.text = stringFromTimeInterval(interval: Double((isnil(json: imagePath, key: "file_duration")))!)*/
            }
            else
            {
                if isPhoto == true
                {
                    textView.isHidden = false
                    backgroundimg.kf.indicatorType = .activity
                    backgroundimg.kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["large"] as! String))))
                    
                 /*   movieTitle.text = (imagePath["title"] as! String)
                    movieYear.text = ""
                    movieTime.text = ""*/
                }
                else
                {
                    textView.isHidden = false
                    backgroundimg.kf.indicatorType = .activity
                    backgroundimg.kf.setImage(with: URL(string: "http://s3.amazonaws.com/peafowl/aeom/cover.jpg"))
                   /* let Path = storeProdData.first! as NSDictionary
                    let carousel = Path["carousels"] as! NSArray
                    let data = carousel.firstObject as! NSDictionary
                    movieTitle.text = (data["carousel"] as! String)
                    movieYear.text = ""
                    movieTime.text = (Path["duration"] as! String)*/
                }
            }
            break
        default:
            break
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // listcollectio view update focus
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        textView.isHidden = false
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
          //  let imagePath = menuCollectionList[previousIndexPath.row] as! NSDictionary
           // let image = imagePath["metadata"] as! NSDictionary
           //  backgroundimg.kf.setImage(with: URL(string: isnil(json: image, key: "main_carousel_image_url")))
            cell.transform = CGAffineTransform.identity
        }
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            likeView.isHidden = false
            let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
            if imagePath["metadata"] != nil
            {
                let image = imagePath["metadata"] as! NSDictionary
                backgroundimg.kf.setImage(with: URL(string: isnil(json: image, key: "main_carousel_image_url")))
                collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
                (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                movieTitle.text = (isnil(json: imagePath, key: "name")).capitalized
                movieTitle?.frame = CGRect(x: (movieTitle?.frame.origin.x)!, y: (movieTitle?.frame.origin.y)!, width: ((movieTitle.text)?.widthWithConstrainedWidth(height: 60, font: (movieTitle?.font)!))!, height: (movieTitle?.frame.size.height)!)
                let str1 = ((isnil(json: image, key: "release_date")).components(separatedBy: "-"))
                movieYear.text = str1[0]
                movieTime.text = stringFromTimeInterval(interval: Double((isnil(json: imagePath, key: "file_duration")))!)
            }
            else
            {
                if isPhoto == true
                {
                    textView.isHidden = false
                    backgroundimg.kf.indicatorType = .activity
                    backgroundimg.kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["large"] as! String))))
                    (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                    movieTitle.text = (imagePath["title"] as! String)
                    movieYear.text = ""
                    movieTime.text = ""
                }
                else
                {
                    textView.isHidden = false
                    backgroundimg.kf.indicatorType = .activity
                    backgroundimg.kf.setImage(with: URL(string: "http://s3.amazonaws.com/peafowl/aeom/cover.jpg"))
                    (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                    let Path = storeProdData.first! as NSDictionary
                    let carousel = Path["carousels"] as! NSArray
                    let data = carousel.firstObject as! NSDictionary
                    movieTitle.text = (data["carousel"] as! String)
                    movieYear.text = ""
                    movieTime.text = (Path["duration"] as! String)

                }
            }
            
        }
    }
    
    // did select list collection view
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let path = menuCollectionList[indexPath.row] as! NSDictionary
        if path["metadata"] != nil
        {
            getaccountInfo(id:path["id"] as! String,userid:UserId/*,tvshow:(path["tv_show"] as! Bool)*/)
            //  getAssetData(withUrl:kAssestDataUrl,id: path["id"] as! String,userid: UserId,tvshow:(path["tv_show"] as! Bool))
        }
        else
        {
           if isPhoto == true
           {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let PhotoPage = storyBoard.instantiateViewController(withIdentifier: "Photos") as! AEPhotosViewController
            PhotoPage.PhotoDict = self.PhotoDict
            PhotoPage.ImageIndexpath = indexPath.row
            self.navigationController?.pushViewController(PhotoPage, animated: true)
           }
            else
           {
            let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
            let StorePage = storyBoard.instantiateViewController(withIdentifier: "Store") as! AEStoreViewController
            let storeDict = storeProdData.first! as NSDictionary
            StorePage.prodData = storeDict
            StorePage.userId = UserId
            StorePage.uuid = uuid
            StorePage.deviceID = deviceId
            self.navigationController?.pushViewController(StorePage, animated: true)
           }
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberofcolumns = CGFloat(4)
        let totalwidth = listcollectionview.frame.width
        let itemWidth = ((totalwidth - (numberofcolumns - 1)) / numberofcolumns)
        return CGSize(width: itemWidth, height: 230)
    }
    
    
    // Service call of get assetdata
    func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let DetailPage = self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! AEDetailPageViewController
                DetailPage.TvshowPath = JSON as! NSDictionary
                DetailPage.delegate = self.categoryDelegate
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                self.navigationController?.pushViewController(DetailPage, animated: true)
                //   self.present(DetailPage, animated: true, completion: nil)
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func getaccountInfo(id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id:id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func myListdata(mylistDict: NSDictionary)
    {
    }
    func removeListdata(id : String)
    {
        
    }
    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
        
    }
}

extension AECategoriesViewController
{
    internal func categoriesSelected(carousalName: String, carousalDetailDict: [[String : Any]]) {
        
        self.menuCollectionList.removeAllObjects()
        let carousal = carousalName
        
        for dict in carousalDetailDict
        {
            if carousal == "Photos"
            {
              self.menuCollectionList.add(dict)
            }
            else if carousal == "Store"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                let metaDict = dict["metadata"] as! NSDictionary
                if (metaDict["carousel_id"] as! String) == carousal
                {
                    self.menuCollectionList.add(dict)
                }
            }
        }
        self.listcollectionview.isHidden = false
        self.listcollectionview.reloadData()
    }
}
