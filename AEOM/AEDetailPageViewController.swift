//
//  ASDetailPageViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import AVKit

protocol myListdelegate: class
{
    func myListdata(mylistDict:NSDictionary)
    func removeListdata(id: String)
    func recentlyWatcheddata(recentlyWatchedDict:NSDictionary)
    func likesCount(likesDict:NSDictionary)
}
class AEDetailPageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,playerdelegate {
    
    var EpisodesPath = [[String:Any]]()
    var TvshowPath = NSDictionary()
    var userid = String()
    var deviceId,uuid: String!
    var Menus = [String]()
    var isMain = Bool()
    var isRecentlywatch = Bool()
    var iswatchedVideo = Float64()
    var seektime = Float64()
    var tvshowCount = Int()
    var seektimevideo = Float64()
    var subscription_status = String()
    var statusDict = NSDictionary()
    var isMyList = Bool()
    var VideoID = String()
    var dataUrl = String()
    var isUpdated = Bool()
    var fromSearch = Bool()
    var tvshow = Bool()
    weak var delegate:myListdelegate?
    @IBOutlet var likeLbl: UILabel!
    @IBOutlet var disLikeLbl: UILabel!
    
    @IBOutlet weak var BackgrndImg: UIImageView!
    @IBOutlet weak var TvLbl: UILabel!
    @IBOutlet weak var TimeLbl: UILabel!
    @IBOutlet weak var ReleaseLbl: UILabel!
    @IBOutlet weak var descriptiontxt: UITextView!
    @IBOutlet var operationsListView:UITableView!
    @IBOutlet var starView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //accountStatus()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (TvshowPath["tv_show"] != nil)
        {
            if ((TvshowPath["tv_show"]as! Bool == true) && ((TvshowPath["contains"] as! NSDictionary).count != 0))
            {
                tvshowCount = getTvShowCount(isTvShow: true)
                tvshow = true
            }
            else
            {
                tvshowCount = getTvShowCount(isTvShow: false)
                tvshow = false
            }
        }
        else
        {
            tvshowCount = getTvShowCount(isTvShow: false)
            tvshow = false
        }
        starView.isUserInteractionEnabled = false
        BackgrndImg.kf.indicatorType = .activity
        let metaDict = TvshowPath.object(forKey: "metadata") as! NSDictionary
    
        BackgrndImg.kf.setImage(with: URL(string: isnil(json: metaDict, key: "main_carousel_image_url")))
        TvLbl.text = (isnil(json:TvshowPath,key:"name")).capitalized
        let labelText = TvLbl.text?.capitalized
        TvLbl?.frame = CGRect(x: (TvLbl?.frame.origin.x)!, y: (TvLbl?.frame.origin.y)!, width: (labelText?.widthWithConstrainedWidth(height: 60, font: (TvLbl?.font)!))!, height: (TvLbl?.frame.size.height)!)
        TimeLbl.text =  stringFromTimeInterval(interval: Double(isnil(json:TvshowPath,key:"file_duration"))!)
        let str1 = (isnil(json: metaDict, key: "release_date")).components(separatedBy: "-")
        ReleaseLbl.text = str1[0]
        descriptiontxt.text = isnil(json: TvshowPath, key: "description")
        
        let DescriptionText = TvshowPath["description"] as? String
        let destxt = DescriptionText?.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
        let destxt1 =  destxt?.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
        let destxt2 = destxt1?.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
        descriptiontxt.text = destxt2
        descriptiontxt?.frame = CGRect(x: (descriptiontxt?.frame.origin.x)!, y: (descriptiontxt?.frame.origin.y)!, width: (descriptiontxt?.frame.size.width)!, height: (DescriptionText?.widthWithConstrainedWidth(height: 60, font: (descriptiontxt?.font)!))!)
        operationsListView.reloadData()
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tvshowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(11) as! UILabel).text = Menus[indexPath.row]
        (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
        cell.selectionStyle = .none
        cell.layer.cornerRadius = 7.0
        //Seektime > 0
        if  TvshowPath["watchedVideo"] as! Float64 > 0.0
        {
            switch indexPath.row
            {
            case 0:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                break
            case 1:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                break
            case 2:
                if TvshowPath["myList"] as! Bool == false
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                }
                break
            case 3:
                if tvshow == true
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                }
                break
            case 4:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                break
            default:
                break
            }
        }
            //  Seektime < 0
        else
        {
            switch indexPath.row
            {
            case 0:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                break
            case 1:
                if TvshowPath["myList"] as! Bool == false
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                }
                break
            case 2:
                if tvshow == true
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                }
                break
            case 3:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
            default:
                break
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        // previous focus view
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previousIndexPath)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
            if  TvshowPath["watchedVideo"] as! Float64 > 0.0
            {
                switch previousIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 2:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 3:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                       (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    }
                    break
                case 4:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    break
                default:
                    break
                }
            }
            else
            {
                switch previousIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    }
                    break
                case 3:
                    
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    break
                default:
                    break
                }
            }
        }
        // next focus view
        if let nextIndexPath = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: nextIndexPath)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
            if  TvshowPath["watchedVideo"] as! Float64 > 0.0
            {
                switch nextIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 1:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 2:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    }
                    break
                case 3:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    }
                    break
                case 4:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    break
                default:
                    break
                }
            }
            else
            {
                switch nextIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 1:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    }
                    break
                case 3:
                    
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    break
                default:
                    break
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! AEPlayerLayerViewController
        let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! AESubscriptionViewController
        if  TvshowPath["watchedVideo"] as! Float64 > 0.0
        {
            switch indexPath.row
            {
            case 0:
                
               // if (TvshowPath["subscription_status"] as! String) == "active"
             //   {
                    PlayerVC.isResume = true
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                 
                    PlayerVC.resumeTime = (TvshowPath["watchedVideo"] as! Float64)
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.isMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    
            
                   // self.navigationController?.pushViewController(PlayerVC, animated: true)
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
                    
            //    }
//                else
//                {
//                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
//                }
//                
                break
            case 1:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.isMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
                    
                      //  self.navigationController?.pushViewController(PlayerVC, animated: true)
                    

       //         }
//                else
//                {
//                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
//                }
                break
            case 2:
                ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["id"] as! String))
                break
            case 3:
                if tvshow
                {
                    let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! AEEpisodesViewController
                    StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                    StoryBoard.UserID = self.userid
                    StoryBoard.videoId = TvshowPath["id"] as! String
                    StoryBoard.navigationController?.isNavigationBarHidden = true
                    StoryBoard.episodeDelegate = delegate
                    self.present(StoryBoard, animated: true, completion: nil)
                }
                else
                {
                    getLikesCount(forPath:indexPath)
                }
                break
            case 4:
                getLikesCount(forPath:indexPath)
                break
            default:
                break
            }
        }
        else
        {
            switch indexPath.row
            {
            case 0:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                        print(TvshowPath["url_m3u8"] as! String)

                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                   
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    PlayerVC.userID = userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    PlayerVC.deviceID = deviceId
                    PlayerVC.isMyList = isMyList
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        
                      self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        
                        self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
            //  }
             /*   else
                {
                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
                }*/
                break
            case 1:
                ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["id"] as! String))
                break
            case 2:
                if tvshow == true
                {
                    let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! AEEpisodesViewController
                    StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                    StoryBoard.UserID = self.userid
                    StoryBoard.videoId = TvshowPath["id"] as! String
                    StoryBoard.episodeDelegate = delegate
                    StoryBoard.navigationController?.isNavigationBarHidden = true
                    
                    self.present(StoryBoard, animated: true, completion: nil)
                }
                else
                {
                    getLikesCount(forPath:indexPath)
                }
              
                break
            case 3:
                getLikesCount(forPath:indexPath)
                
                break
            default:
                break
            }
        }
        PlayerVC.detdelegate = self
    }
    
    
    // Finding TVShow or not
    func getTvShowCount(isTvShow:Bool)->Int
    {
        Menus.removeAll()
        if  TvshowPath["watchedVideo"] as! Float64 > 0.0
        {
            Menus.append("Resume playing")
        }
        if isTvShow
        {
            Menus.append("Play")
            if TvshowPath["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("More Episodes")
            Menus.append("Like this title")
        }
        else
        {
            Menus.append("Play")
            if TvshowPath["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("Like this title")
        }
        return Menus.count
    }
    
    func getLikesCount(forPath:IndexPath)
    {
        let parameters = ["Likes": ["id": TvshowPath["id"] as! String, "type":"typeAsset","userId":self.userid,"count":"1"]]
        
        AEApiManager.sharedManager.postDataWithJson(url:kLikesUrl , parameters: parameters as [String : [String : AnyObject]])
            {(responseDict,error,isDone)in
            if error == nil
            {
              let JSON = responseDict
              let dict = JSON as! NSDictionary
              let likeDict = dict.value(forKey: "likes") as! NSDictionary
              let cell = self.operationsListView.cellForRow(at: forPath)
              if likeDict["userId"] != nil
              {
                self.likeLbl.text = "1"
                self.disLikeLbl.text = "0"
                (cell?.viewWithTag(11) as! UILabel).text = "DisLike this title"
                (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "dislike_gray")
              }
              else
              {
                self.likeLbl.text = "0"
                self.disLikeLbl.text = "1"
                (cell?.viewWithTag(11) as! UILabel).text = "Like this title"
                (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
              }
             // self.delegate?.likesCount(likesDict: JSON as! NSDictionary)
            }
            else
            {
                
            }
            
        }
    }
    
    
    // Service Call for Create Recently Watched
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":TvshowPath["id"]! as AnyObject,"userId":userid as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
                if (dicton["seekTime"] as! Float64) > 0
                {
                    self.isRecentlywatch = true
                }
                self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
                print("json error")

            }
        }
    }
    
    func callwatchlist(dict: NSDictionary) {
        self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dict)
    }
    
    // Service call for ManageMyList
    func ManageList(withurl:String,forPath:IndexPath,assetid:String)
    {
        let parameters = ["manageMyList":["videoId":(TvshowPath["id"])!as AnyObject,"userId":userid as AnyObject,"data":TvshowPath as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error,isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                let dict = JSON["myList"] as! NSDictionary
                let cell = self.operationsListView.cellForRow(at: forPath)
                if (dict["data"] != nil)
                {
                    (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    self.delegate?.myListdata(mylistDict: dict)
                    
                }
                else
                {
                    (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    self.delegate?.removeListdata(id: assetid)
                }
            }
            else
            {
                print("json error")

            }
        }
    }
    
   /* func getAssetData(withUrl:String,id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    self.TvshowPath = dict as! NSDictionary
                }
            }
            else
            {
                print("json Error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            self.operationsListView.reloadData()
        }
    }*/
    
    func accountStatus()
    {
        let url = kAccountInfoUrl
        
        let  parameters = [ "getAccountInfo": ["deviceId": self.deviceId!, "uuid": self.uuid!]]
        print(parameters)
        AEApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                self.statusDict = post as! NSDictionary
                //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                //                accountInfo.accountDict = dict
                //                self.navigationController?.pushViewController(accountInfo, animated: true)
            }
            else
            {
                print("json error")

            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getassetdata(withUrl:String,id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        print("Iam in play delegate getasset data")
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                self.TvshowPath = JSON as! NSDictionary
             
            }
            else
            {
                print("json Error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                    let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
}




