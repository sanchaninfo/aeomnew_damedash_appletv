//
//  AppDelegate.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var isFirstTime:Bool?
    var deviceId,uuid,userId,status,code:String!
    var bundleID = "deviceId"
    var venderId: String!
    var uuidofDevice:String!
    var udid = UIDevice.current.identifierForVendor?.uuidString
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        gotoCode()
        return true
    }
    
    func gotoCode()
    {
        
        if isConnectedToNetwork() == false
        {
//            let alertView = SPCustomAlertView(frame: CGRect(x: 0, y: 0, width: 1920, height: 1080))
//            alertView.setTitle(message: "No Network Detected Please check the Internet connection")
//            self.window?.makeKeyAndVisible()
//            self.window?.addSubview(alertView)
            let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                UIAlertAction in
                exit(0)
            })
            alertview.addAction(defaultAction)
            self.window?.makeKeyAndVisible()
            self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
        }
        else
        {
            let activityView = ASActivityView.init(frame:(self.window?.frame)!)
            self.window?.addSubview(activityView)
//            self.uuid = "f2636278-2735-4b5a-8603-a9f3ee74e1d0"
//            self.deviceId = "0B321F7F-16F8-40C7-9A19-CF23B4FADD73"
//         //   self.userId = "58e2479fd51327a30e4e9524"
//            self.userId = "5912ee55b531631224143dc3"
//            gotoMenu(userid: self.userId, deviceid: self.deviceId)
//            activityView.removeFromSuperview()
            var parameters = [String:[String:AnyObject]]()
            
            venderId = getVenderId()
            
            if venderId != ""
            {
                // uuidofDevice = venderId
            }
            else
            {
                setVenderId()
            }
            parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":venderId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject]]
            AEApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
                if error == nil
                {
                    let dict = responseDict as! [String:String]
                    print(dict)
                    self.uuid = dict["uuid"]
                    self.deviceId = dict["device_id"]
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    if !((dict["pair_devce"]! as String).contains("active"))
                    {
//                        let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! AECodeViewController
//                        rootView.uuid = self.uuid
//                        rootView.deviceId = self.deviceId
//                        rootView.code = dict["code"]
                        
                        let rootView = storyBoard.instantiateViewController(withIdentifier: "AEOMCode") as! AEOMCodeViewController
                        rootView.uuid = self.uuid
                        rootView.deviceId = self.deviceId
                        rootView.code = dict["code"]
                        let navigationController = UINavigationController(rootViewController: rootView)
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                    else
                    {
                        let uservalue = self.getUserId()
                        if (uservalue != "")
                        {
                            self.userId = uservalue
                            self.getaccountInfo()
                          //  self.gotoMenu(userid: self.userId,deviceid: self.deviceId)
                        }
                    }
                }
                else
                {
                    let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                        UIAlertAction in
                    })
                    alertview.addAction(defaultAction)
                    self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
                }
                
                activityView.removeFromSuperview()
            }
        }
    }
    
    //setVenderId
    func setVenderId() {
        let keychain = Keychain(service: bundleID)
        do {
            try keychain.set(udid!, key: bundleID)
            print("venderId set : key \(bundleID) and value: \(udid)")
            venderId = getVenderId()
        }
        catch let error {
            print("Could not save data in Keychain : \(error)")
        }
        print(venderId)
    }
    
    
    // getVenderId
    func getVenderId() -> String {
        
        let keychain = Keychain(service: bundleID)
        
        if try! keychain.get(bundleID) != nil
        {
            let token : String = try! keychain.get(bundleID)!
            
            return token
            
        }
        return ""
        
    }
    //getUserid
    func getUserId() -> String {
        
        let keychain = Keychain(service: "userId")
        
        if try! keychain.get("userId") != nil
        {
            let token : String = try! keychain.get("userId")!
            
            return token
        }
        return ""
        
    }
    func getaccountInfo()
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        
        AEApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                print(dict)
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    //   self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(dict.value(forKey: "subscription_status") as! String))
                    self.gotoMenu(userid: self.userId,deviceid: self.deviceId)
                }
                else
                {
                    //                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    //                    self.fromaccount = true
                    //                    appDelegate.gotoCode()
                    parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":self.udid! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject]]
                    AEApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
                        if error == nil
                        {
                            let dict = responseDict as! [String:String]
                            self.uuid = dict["uuid"]
                            self.deviceId = dict["device_id"]
                            self.code = dict["code"]
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let rootView = storyBoard.instantiateViewController(withIdentifier: "AEOMCode") as! AEOMCodeViewController
                            rootView.uuid = self.uuid
                            rootView.deviceId = self.deviceId
                            rootView.code = self.code
                            let navigationController = UINavigationController(rootViewController: rootView)
                            self.window?.rootViewController = navigationController
                            self.window?.makeKeyAndVisible()
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    
    // go to Landing controller
    func gotoMenu(userid:String,deviceid:String)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Main") as! AEMainViewController
        rootView.deviceId = deviceid
        rootView.userId = userid
        rootView.uuid = self.uuid
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

