//
//  AENocardViewController.swift
//  AEOM
//
//  Created by Vijay Bhaskar on 13/05/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class AENocardViewController: UIViewController {
    @IBOutlet var gotoLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        gotoLbl.text = "Go to \(kBaseUrl)"
        // Do any additional setup after loading the view.
    }
    @IBAction func okbtn(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
